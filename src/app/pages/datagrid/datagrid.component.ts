import { Component, OnInit, ViewChild } from '@angular/core';
// import { Service } from './app.service';
import { data } from 'src/assets/json/data';
import { DxDataGridComponent } from "devextreme-angular";
import { LocalService } from 'src/app/shared/services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-datagrid',
  templateUrl: './datagrid.component.html',
  styleUrls: ['./datagrid.component.scss'],
  // providers: [Service]
})
export class DatagridComponent implements OnInit {
  @ViewChild("grid") dataGrid: DxDataGridComponent

  dataSource: any[];
  readonly allowedPageSizes = [5, 10, 'all'];
  displayMode = "full";
  showPageSizeSelector = true;
  showInfo = true;
  showNavButtons = true;
  customizeColumns(columns) {
    // columns[0].width = 170;
  }
  showFilterRow: boolean = true;
  showHeaderFilter: boolean = true;
  currentFilter: any = 'auto';

  constructor(
    private local: LocalService,
    private router: Router,
  ) {
    // this.dataSource = service.getCustomers();
    // this.dataSource = data;
  }

  editData(args: any): void {
    let data = args.data;
    console.log(data);
    this.local.setLocalSingle('syncDataSingle', data);
    this.router.navigateByUrl('pages/detail');
  }
  deleteData(args: any): void {
    let data = args.data;
    console.log(data);
    let syncData = this.local.getLocalSingle('syncData');
    syncData = syncData.filter((obj: any) => obj.OrderId != data.OrderId);
    // console.log(syncData);
    this.local.setLocalSingle('syncData', syncData);
    this.dataSource = syncData;
  }

  ngOnInit(): void {
    this.local.removeLocalSingle('syncDataSingle');
    let syncData = this.local.getLocalSingle('syncData');
    if (syncData) {
      this.dataSource = syncData;
    } else {
      this.dataSource = data;
      this.local.setLocalSingle('syncData', data);
    }

    // grid.instance.getSelectedRowsData()[0]
  }

}
