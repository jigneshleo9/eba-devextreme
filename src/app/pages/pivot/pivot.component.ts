import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DxPivotGridComponent, DxChartComponent, DxDataGridComponent } from 'devextreme-angular';
import { LocalService } from 'src/app/shared/services';
import { data } from 'src/assets/json/data';

@Component({
  selector: 'app-pivot',
  templateUrl: './pivot.component.html',
  styleUrls: ['./pivot.component.scss']
})
export class PivotComponent implements OnInit {
  @ViewChild(DxPivotGridComponent, { static: false }) pivotGrid: DxPivotGridComponent;
  @ViewChild(DxChartComponent, { static: false }) chart: DxChartComponent;
  @ViewChild("drillDownDataGrid") drillDownDataGrid: DxDataGridComponent;

  pivotGridDataSource: any;
  dataSource: any[];
  drillDownDataSource: any;
  salesPopupVisible = false;
  salesPopupTitle = "";
  showTotalsPrior = false;
  rowsDataFieldArea = false;
  treeHeaderLayout = true;


  constructor(
    private local: LocalService,
    private router: Router,
    private cd: ChangeDetectorRef
  ) {
    this.customizeTooltip = this.customizeTooltip.bind(this);
  }

  ngAfterViewInit() {
    this.pivotGridDataSource = {
      fields: [{
        caption: "Customer ID",
        width: 120,
        dataField: "CustomerID",
        area: "row",
        sortBySummaryField: "Total"
      }, {
        caption: "ShipCountry",
        dataField: "ShipCountry",
        width: 150,
        area: "row",
        selector: this.RegionSelector
      }, {
        dataField: "OrderDate",
        dataType: "date",
        area: "column"
      }, {
        groupName: "OrderDate",
        groupInterval: "month",
        visible: false
      }, {
        caption: "Freight",
        dataField: "Freight",
        dataType: "number",
        summaryType: "sum",
        format: "currency",
        area: "data"
      }],
      store: this.dataSource
    }
    this.pivotGrid.instance.bindChart(this.chart.instance, {
      dataFieldsDisplayMode: "splitPanes",
      alternateDataFields: false
    });
    this.cd.detectChanges();

    // setTimeout(() => {
    //   var dataSource = this.pivotGrid.instance.getDataSource();
    //   dataSource.expandHeaderItem('row', ['CENTC']);
    //   dataSource.expandHeaderItem('column', [1996]);
    // }, 0);
    setTimeout(() => {
      var dataSource = this.pivotGrid.instance.getDataSource();
      console.log(dataSource);
    }, 2000);
  }
  onPivotCellClick(e) {
    console.log(e);

    if (e.area == "data") {
      var rowPathLength = e.cell.rowPath.length,
        rowPathName = e.cell.rowPath[rowPathLength - 1];
      // console.log(this.pivotGridDataSource);

      // this.drillDownDataSource = this.pivotGridDataSource.createDrillDownDataSource(e.cell);
      // this.salesPopupTitle = (rowPathName ? rowPathName : "Total") + " Drill Down Data";
      // this.salesPopupVisible = true;
    }
  }

  onPopupShown() {
    this.drillDownDataGrid.instance.updateDimensions();
  }
  RegionSelector(data) {
    return data.ShipCountry + ' (' + data.ShipRegion + ')';
  }
  customizeTooltip(args) {
    return {
      html: args.seriesName + " | Total<div class='currency'>" + args.valueText + "</div>"
    };
  }
  ngOnInit() {
    this.local.removeLocalSingle('syncDataSingle');
    let syncData = this.local.getLocalSingle('syncData');
    if (syncData) {
      this.dataSource = syncData;
    } else {
      this.dataSource = data;
      this.local.setLocalSingle('syncData', data);
    }

  }

}
