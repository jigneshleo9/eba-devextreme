
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { AuthService, LocalService } from 'src/app/shared/services';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  loading = false;

  editData = false;
  overObj: any = {
    "OrderId": null,
    "CustomerID": "",
    "EmployeeID": null,
    "Freight": null,
    "OrderDate": "",
    "ShipAddress": "",
    "ShipCity": "",
    "ShipCountry": "France",
    "ShipName": "",
    "ShipPostalCode": "",
    "ShipRegion": ""

    // "OrderId": 123,
    // "CustomerID": "Jignesh",
    // "EmployeeID": 20,
    // "Freight": 50,
    // "OrderDate": "1996-07-03T18:30:00.000Z",
    // "ShipAddress": "59 rue de l Abbaye",
    // "ShipCity": "Reims",
    // "ShipCountry": "France",
    // "ShipName": "Vins et alcools Chevalier",
    // "ShipPostalCode": "51100",
    // "ShipRegion": "CJ"
  };
  ShipCountryOptions: string[] = [
    "France",
    "Germany",
    "Brazil",
    "Belgium",
    "Switzerland"
  ];

  autocomplete: any = true;

  constructor(
    private local: LocalService,
    private router: Router,
    private authService: AuthService,
  ) { }

  buttonOptions: any = {
    text: "Submit",
    type: "success",
    useSubmitBehavior: true
  }

  async onSubmit(e) {
    e.preventDefault();
    this.loading = true;
    console.log(this.overObj);


    let syncData = this.local.getLocalSingle('syncData');
    if (!this.editData) {
      syncData.unshift({ ...this.overObj });
    } else {
      let objIndex = syncData.findIndex(((obj: any) => obj.OrderId == this.overObj.OrderId));
      syncData[objIndex] = { ...this.overObj }
    }
    // console.log(syncData);

    this.local.setLocalSingle('syncData', syncData);

    this.router.navigateByUrl('pages/datagrid');
    this.loading = false;
  }

  ngOnInit(): void {

    let data = this.local.getLocalSingle('syncDataSingle');
    if (data) {
      this.overObj = data;

      // this.overObj.OrderDate = new Date(data.OrderDate);
      // console.log(data.OrderDate);
      // console.log(this.overObj.OrderDate);
      this.editData = true;
    } else {
      this.editData = false;
    }
  }

}
