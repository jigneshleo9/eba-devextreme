import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent, ResetPasswordFormComponent, CreateAccountFormComponent, ChangePasswordFormComponent } from './shared/components';
import { AuthGuardService } from './shared/services';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { TasksComponent } from './pages/tasks/tasks.component';
import { DxDataGridModule, DxFormModule, DxPivotGridModule, DxChartModule, DxPopupModule, DxTemplateModule } from 'devextreme-angular';
import { DatagridComponent } from './pages/datagrid/datagrid.component';
import { DetailComponent } from './pages/detail/detail.component';
import { PivotComponent } from './pages/pivot/pivot.component';

const routes: Routes = [
  {
    path: 'pages/pivot',
    component: PivotComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'pages/detail',
    component: DetailComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'pages/datagrid',
    component: DatagridComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'tasks',
    component: TasksComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'login-form',
    component: LoginFormComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'reset-password',
    component: ResetPasswordFormComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'create-account',
    component: CreateAccountFormComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'change-password/:recoveryCode',
    component: ChangePasswordFormComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    redirectTo: 'pages/datagrid'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true }), DxDataGridModule, DxFormModule, DxPivotGridModule, DxChartModule, DxPopupModule, DxTemplateModule],
  providers: [AuthGuardService],
  exports: [RouterModule],
  declarations: [HomeComponent, ProfileComponent, TasksComponent, DatagridComponent, DetailComponent, PivotComponent]
})
export class AppRoutingModule { }
