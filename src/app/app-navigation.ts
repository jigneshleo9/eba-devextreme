export const navigation = [
  // {
  //   text: 'Home',
  //   path: '/home',
  //   icon: 'home'
  // },
  // {
  //   text: 'Examples',
  //   icon: 'folder',
  //   items: [
  //     {
  //       text: 'Profile',
  //       path: '/profile'
  //     },
  //     {
  //       text: 'Tasks',
  //       path: '/tasks'
  //     }
  //   ]
  // },
  {
    text: 'Create Order',
    path: '/pages/detail',
    icon: 'doc'
  },
  {
    text: 'Order List',
    path: '/pages/datagrid',
    icon: 'bulletlist'
  },
  {
    text: 'Pivot',
    path: '/pages/pivot',
    icon: 'inserttable'
  },
];
